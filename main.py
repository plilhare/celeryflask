from flask import Flask, flash, render_template, request, redirect, url_for
from flask_mail import Mail, Message
from celery import Celery

app = Flask(__name__)
app.secret_key = 'very_very_secure_and_secret'
# Flask-Mail
app.config["MAIL_SERVER"] = 'smtp.googlemail.com'
app.config["MAIL_PORT"] = 587
app.config["MAIL_USE_TLS"] = True
app.config["MAIL_USERNAME"] = 'ppp444pratik@gmail.com'
app.config["MAIL_PASSWORD"] = 'ppp444@Pratik'

app.config["CELERY_BROKER_URL"] = 'redis://localhost:6379/0'
app.config["RESULT_BACKEND"] = 'redis://localhost:6379/0'

app.config["CELERY_TASK_SERIALIZER"] = 'json'
app.config["CELERY_RESULT_SERIALIZER"] = 'json'
app.config["CELERY_TIMEZONE"] = 'America/Los_Angeles'
app.config["CELERY_ENABLE_UTC"] = True

app.config["CELERY_IMPORTS"] = ("tasks",)

client = Celery(app.name, broker=app.config['CELERY_BROKER_URL'], include=['.main'])
client.conf.update(app.config)

mail = Mail(app)

@client.task
def send_mail(data):
    """ Function to send emails.
    """
    with app.app_context():
        msg = Message("Ping!",
                    sender="admin.ping",
                    recipients=[data['email']])
        msg.body = data['message']
        mail.send(msg)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')

    elif request.method == 'POST':
        data = {}
        data['email'] = request.form['email']
        data['first_name'] = request.form['first_name']
        data['last_name'] = request.form['last_name']
        data['message'] = request.form['message']
        duration = int(request.form['duration'])
        duration_unit = request.form['duration_unit']

        if duration_unit == 'minutes':
            duration *= 1
        elif duration_unit == 'hours':
            duration *= 3600

        send_mail.apply_async(args=[data], countdown=duration)
        flash(f"Email will be sent to {data['email']} in {request.form['duration']} {duration_unit}")

        return redirect(url_for('index'))






if __name__ == '__main__':
    app.run(debug=True)